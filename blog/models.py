from django.db import models
from django.utils import timezone
from django.shortcuts import reverse


class Post(models.Model):
    TITLE_MAX_LENGTH = 120
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=TITLE_MAX_LENGTH, blank=True, default='', verbose_name='Заголовок')
    body = models.TextField(max_length=9000, default='', verbose_name='Тело сообщения')

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return str(self.body)[:Post.TITLE_MAX_LENGTH]

    @classmethod
    def create(cls, title=None, body=None):
        if title == '' or title is None:
            title = str(body)[:Post.TITLE_MAX_LENGTH]
        post = cls(title=title, body=body)
        return post

    def get_absolute_url(self):
        return reverse('post_detail', kwargs={'pk': int(self.id)})
