from django.urls import path, include


from .views import PostListView, PostDetailView
from .views import programs, precautions, about
from .views import PostCreate, PostUpdate, PostDelete, SignUpView


urlpatterns = [
    path('', PostListView.as_view(), name='index'),
    path('create_post/', PostCreate.as_view(), name='post_create'),
    path('p<int:pk>/', PostDetailView.as_view(), name='post_detail'),
    path('p<int:pk>/update_post/', PostUpdate.as_view(), name='post_update'),
    path('p<int:pk>/delete_post/', PostDelete.as_view(), name='post_delete'),
    path('programs/', programs, name='programs'),
    path('precautions/', precautions, name='precautions'),
    path('about/', about, name='about'),
    # login, logout, sigup, etc
    path('', include('django.contrib.auth.urls')),
    path('signup/', SignUpView.as_view(), name='signup'),
]
