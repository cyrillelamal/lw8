from django import forms
from django.forms import ValidationError
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.conf import settings


class SignUpForm(UserCreationForm):
    # additional fields
    registration_key = forms.CharField(max_length=20, help_text='Получите у Кирюши или найдите в исходниках.', label='Ключ')

    def clean_registration_key(self):
        if settings.REGISTRATION_KEY != self.cleaned_data['registration_key']:
            raise ValidationError('Нужен корректный ключ')
        return self.cleaned_data['registration_key']

    class Meta:
        model = User
        fields = ('registration_key', 'username', 'password1', 'password2',)
