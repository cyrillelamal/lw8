from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.views import View
from django.shortcuts import render, redirect, reverse
from django.urls import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect  # TODO clear
from django.contrib.auth import login
# from django.contrib.auth.forms import UserCreationForm


from .models import Post
from .forms import SignUpForm


class PostListView(ListView):
    """index"""
    # blog/post_list.html
    # object_list or post_list in templates
    model = Post
    # context_object_name = 'posts'
    template_name = 'blog/index.html'


class PostDetailView(DetailView):
    # blog/post_detail.html
    # object or post in template
    model = Post
    # context_object_name = 'post'


def about(request):
    return render(request, 'blog/about.html', {})


def programs(request):
    return render(request, 'blog/programs.html', {})


def precautions(request):
    return render(request, 'blog/precautions.html', {})


class PostCreate(CreateView):
    model = Post
    fields = ['title', 'body']


class PostUpdate(UpdateView):
    model = Post
    fields = ['title', 'body']


class PostDelete(DeleteView):
    model = Post
    success_url = reverse_lazy('index')


class SignUpView(View):
    def post(self, request):
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('index')
        return render(request, 'registration/signup.html', {'form': form})

    def get(self, request):
        form = SignUpForm()
        return render(request, 'registration/signup.html', {'form': form})
